<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('users', function (Blueprint $table) {
            $table->date('birth')->nullable()->after('password');
            $table->integer('city_id')->nullable()->unsigned()->after('password');
            $table->integer('state_id')->nullable()->unsigned()->after('password');
            $table->enum('gender',['M','F','O'])->default('O')->after('password');
            $table->boolean('active')->default(1)->after('password');
            $table->boolean('verified')->default(0)->after('password');
            $table->string('token')->nullable()->after('password');

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->index(['active']);
            $table->index(['verified']);
        });
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_state_id_foreign');
            $table->dropForeign('users_city_id_foreign');

            $table->dropIndex('users_active_index');
            $table->dropIndex('users_verified_index');
            
            $table->dropColumn('birth');
            $table->dropColumn('city_id');
            $table->dropColumn('state_id');
            $table->dropColumn('gender');
            $table->dropColumn('active');
            $table->dropColumn('verified');
        });
    }
}
