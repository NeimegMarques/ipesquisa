<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'cities'], function(){
	Route::get('/', 'CitiesController@index');
});

Route::group(['middleware'=>['auth:api']], function(){

	Route::group(['prefix'=>'vote'], function(){
		Route::post('/', 'VotesController@vote');
	});

	Route::group(['prefix'=>'user'], function(){
		Route::post('missing-info', 'UsersController@fillMissingInfo');
	});
	
	Route::group(['prefix'=>'survey'], function(){
		Route::get('/{uuid}', 'SurveysController@APIshow');
		Route::get('/{uuid}/state/{user_state_id}', 'SurveysController@APIshow');
	});

});