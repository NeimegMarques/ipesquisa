<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', function(){
	return redirect('/');
});

Route::get('/privacidade_e_termos', 'PagesController@privacy');

Route::get('/','PagesController@index');

Route::get('/register/confirm/{token}','PagesController@verifyEmail');

// SOCIALITE
Route::get('facebook', function () {
    return view('facebook');
});
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
// END SOCIALITE

Route::group([], function(){	
	Route::group(['prefix' => 'pesquisa'], function(){
		Route::get('{uuid}','SurveysController@show');
	});
});

Route::group(['middleware'=>'auth'], function(){
	Route::get('/send-verification-email','PagesController@sendVerificationEmail');
});

Route::get('pesq/{uuid}', 'SurveysController@APIshow');

Route::group(['prefix'=>'admin','middleware'=>'auth'], function(){
	Route::get('/','PagesController@index');
});
