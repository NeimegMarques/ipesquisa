
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import swal from 'bootstrap-sweetalert'


var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('quiz-page', require('./front/components/QuizPage.vue'));
Vue.component('register-page', require('./front/components/RegisterPage.vue'));
Vue.component('login-page', require('./front/components/LoginPage.vue'));
Vue.component('reset-page', require('./front/components/ResetPage.vue'));
Vue.component('topbar', require('./front/components/Topbar.vue'));
Vue.component('home', require('./front/components/Home.vue'));
Vue.component('missing-user-info', require('./front/components/MissingUserInfo.vue'));

Vue.mixin({
	props:{
        flash_message:'',
    },
    data(){
        return {
            auth:false,
            verified:false,
            user:null,
            states:null,
            cities:null,
        }
    },
    created(){
        let auth = document.head.querySelector('meta[name="auth"]').content
        let verified = document.head.querySelector('meta[name="verified"]').content
        let user = document.head.querySelector('meta[name="user"]').content
        let states = document.head.querySelector('meta[name="states"]').content

        this.$set(this, 'auth', auth == 1 ? true : false)
        this.$set(this, 'verified', verified == 1 ? true : false)
        this.$set(this, 'user', user != '' ? JSON.parse(user) : null)
        this.$set(this, 'states', JSON.parse(states))
    },
	mounted(){
		if(this.flash_message){
            swal("",this.flash_message, 'success')
        }
    },
    methods:{
        validEmail:function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    }
})


const app = new Vue({
    el: '#app',
    mounted(){
        console.log(this.props)

        if(this.user && this.user.name && (!this.user.state_id || !this.user.city_id || !this.user.birth) ){
            $('#modal-missing-info').modal({
                show:true,
                backdrop:'static',
                keyboard:false,
            })
        }
    },
    methods:{
    	sendVerifEmailClicked(event){
    		$(event.target).hide()
    	},
    }
});
