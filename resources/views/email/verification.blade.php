<!DOCTYPE html>
<html>
<head>
    <title>Ative sua conta</title>
    <style type="text/css" rel="stylesheet">
    	.btn{
    		padding: 12px 18px;
    		border:0;
    		background:blue;
    		color:#fff;
    		font-size: 16px;
    		text-decoration: none;
    	}
    </style>
</head>
 
<body>
	<h2>Olá {{explode(' ',$user['name'])[0]}}</h2>
	<br/>
	<p>Clique no botão <a href="{{url('/register/confirm/'.$user['token'])}}"><b>ativar conta</b></a> para poder votar e ver os resultados das pesquisas.</p>
	<br>
	<p><a class="btn" href="{{url('/register/confirm/'.$user['token'])}}">Ativar conta</a></p>
</body>
 
</html>