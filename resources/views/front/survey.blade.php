@extends('layouts.app', ['component'=>'quiz-page',
    'props' => [
        'scenarios_count'=>$survey->scenarios->count(),
        'survey_id' => $survey->id,
        'survey_uuid' => $survey->uuid,
        'campaign' => $survey->campaign->id,
        'hasVoted'=>$voted,
    ],
    'meta' => [
        'title' => $survey->question,
        'description' => 'Ajude-nos a descobrir qual candidato tem mais chances de tornar-se presidente do Brasil em 2018. Vote!',
        'url' => url('/pesquisa/' . $survey->uuid),
    ]
])

@section('content')
<section class="quiz-container">
    <div class="question-container" style="background-image:url({{asset('/img/planalto.jpg')}})">
        <div class="question-container-inner">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col">
                        <h1 class="question">
                            <b>{{$survey->question}}</b>
                        </h1>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus ex eligendi aut debitis rem vero reprehenderit, atque delectus recusandae nostrum quaerat cum nihil blanditiis aperiam enim officiis, accusamus. Error, reiciendis.</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-2 pb-5">
        <div class="container">
            <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-1955889356284494"
             data-ad-slot="4179029457"
             data-ad-format="auto"></ins>
        </div>
    </div>
    <div class="container quiz-options-container">
        <div class="row text-center">
            <div class="col">
                <div class="alert alert-info" style="margin-bottom: 40px; color: #fff;background-color: #00c; border:0;"><b>Para melhor percepção do atual cenário político, a contagem dos votos é reiniciada no início de cada semana.</b></div>
            </div>
        </div>
        <div class="row text-center justify-content-center">
            <div class="col">
                @if( !$voted )
                <template v-if="!voted">
                    @foreach($survey->scenarios as $i=>$scenario)
                    <div class="scenario" id="{{'scenario_' . $scenario->id}}">
                        <h3 class="scenario-title">
                            <b>
                                @if($survey->scenarios->count() > 1)
                                    CENÁRIO {{$i+1}}
                                @endif
                            </b>
                        </h3>
                        @if(count($scenario->excluded_ids))
                        <p>Sem <?php
                            $eids_length = count($scenario->excluded_ids);
                            foreach ($scenario->excluded_ids as $eid_index => $eid_value) {
                                echo '<b>' . $survey->candidates->firstWhere('id',$eid_value)->name;
                                
                                if($eids_length > $eid_index + 1){
                                    if($eids_length - 1 == $eid_index + 1){
                                        echo ' </b>e <b>';
                                    } else {
                                        echo '</b>, <b>';
                                    }
                                }

                                echo '</b>';
                            }
                        ?></p>
                        @endif
                        <div class="row justify-content-center">
                            @foreach($survey->candidates->shuffle() as $candidate)
                            <div class="col-md-3 col-sm-6 col-8 quiz-option {{in_array($candidate->id, $scenario->excluded_ids) ? 'd-none':''}}" :class="{'voted':selected_options.scenario_{{$scenario->id}} == {{$candidate->id}} }">
                                <div class="quiz-option-img-container" @click="selectOption({{$scenario->id}},{{$candidate->id}})">
                                    <img class="rounded-circle img-fluid" src="{{asset('/img/candidates/' .  $candidate->image)}}" alt="">
                                    <div class="quiz-option-vote-button-container d-none d-sm-block rounded-circle">
                                        <button class="quiz-option-vote-button">Votar</button>
                                    </div>
                                </div>
                                <div class="quiz-option-name">{{$candidate->name}}</div>
                                <div class="quiz-option-brand">{{$candidate->brand}}</div>
                                <div class="quiz-option-vote-button-container-xs d-block d-sm-none">
                                    <button @click="selectOption({{$scenario->id}},{{$candidate->id}})" class="btn btn-purple quiz-option-vote-button-xs">Votar</button>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                    <br>
                    <button :disabled="voting" @click="doConfirm()" id="btn-confirm"
                        v-if=" !voted && _.size(this.selected_options) == parseInt(this.scenarios_count)" 
                        class="btn btn-success btn-confirm btn-lg">
                        <span v-if="voting">Confirmando...</span>
                        <span v-else>Confirmar</span>        
                    </button>
                    <br>
                </template>
                <div class="container-fluid mb-5">
                    <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1955889356284494"
                     data-ad-slot="4179029457"
                     data-ad-format="auto"></ins>
                </div>
                @endif
                <ul class="social-share nav justify-content-center">
                    <li class="nav-item facebook">
                        <a class="nav-link active" href="#" 
                            onclick="
                            window.open(
                            'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
                            'facebook-share-dialog', 
                            'width=626,height=436'); 
                            return false;">
                            <i class="fa fa-facebook"></i>
                        </a>

                    </li>
                    <li class="nav-item twitter">
                        <a href="http://twitter.com/share" @click.prevent="twitterShareClicked" class="nav-link active">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="results" ref="results_container" :class="{'scenario-selector-fixed': scenario_selector_fixed}">
        <template v-if="loading_results">
            <div class="container">
                <br>
                <br>
                <br>
                <br>
                <br>
                <div id="spinner"></div>
            </div>
        </template>
        <template v-if="!loading_results && survey">
            <div class="pt-5 pb-1 bg-grey">
                <div class="container">
                    <div class="row justify-content-center mb-3">
                        <div class="col-sm-8">
                            <div class="btn-group btn-block">
                                <button :disabled="loading_results" type="button" class="btn btn-default btn-block btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    @{{filter_name}}
                                </button>
                                <div class="dropdown-menu">
                                    <a @click.prevent="setFilter('TODO O BRASIL')" class="dropdown-item" href="#">TODO O BRASIL</a>
                                    <a @click.prevent="setFilter('POR ESTADO')" class="dropdown-item" href="#">FILTRAR POR ESTADO</a>
                                    <a @click.prevent="setFilter('POR CIDADE')" class="dropdown-item" href="#">FILTRAR POR CIDADE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-if="filter_name == 'POR ESTADO' || filter_name == 'POR CIDADE'" class="row justify-content-center mb-3">
                        <div class="col-sm-8">
                            <select ref="state_selector" v-model="state_id" id="" class="form-control form-control-lg">
                                <option disabled value="0">SELECIONE O ESTADO</option>
                                @foreach(\App\Models\State::all() as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div v-if="filter_name == 'POR CIDADE'" class="row justify-content-center mb-3">
                        <div class="col-sm-8">
                            <select ref="city_selector" :disabled="state_id == 0 && filter_name == 'POR CIDADE'" v-model="city_id" id="" class="form-control form-control-lg">
                                <option disabled value="0">@{{loading_cities ? 'Carregando as cidades...':'SELECIONE A CIDADE'}}</option>
                                <option v-for="city in cities" :value="city.id">@{{city.name}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-4 pb-1 bg-grey">
                <div class="container">
                    <div class="row justify-content-center mb-5">
                        <div class="col-sm-24">
                            <ul class="nav nav-tabs justify-content-center scenario-selector" :class="{'scenario-selector-fixed': scenario_selector_fixed}">
                                <li class="nav-item" v-for="(scenario,ind) in survey.scenarios">
                                    <a class="nav-link" :class="{'active':visible_scenario == scenario.id}" href="#" @click.prevent="setVisibleScenario(scenario.id)">
                                        Cenário @{{ind + 1}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <template v-for="(scenario, i) in survey.scenarios" v-if="visible_scenario == scenario.id">
                <div class="py-8 pt-1 bg-grey">
                    <div class="container">
                        <div class="row text-center justify-content-center mb-5">
                            <h4>
                                
                                Este cenário não inclui <span v-for="(candidate, key) in _.filter(survey.candidates, (candidate) => { return _.indexOf(scenario.excluded_ids ,candidate.id) >= 0 })"
                                >
                                    <b>
                                        @{{candidate.name}}
                                    </b>
                                    <template v-if="scenario.excluded_ids.length > key + 1">
                                        <template v-if="scenario.excluded_ids.length - 1 == key + 1">e&nbsp;</template>
                                        <template v-else>, </template>
                                    </template>
                                </span>
                            </h4>
                        </div>
                        <div class="row justify-content-center text-center">
                            <div class="col title">
                                <h2 class="mb-5"><b>@{{chosen_city_name ? chosen_city_name : chosen_state_name ? chosen_state_name : 'Resultado geral'}}</b></h2>
                                <h5>@{{scenario.total_votes_equalized > 1 ? scenario.total_votes_equalized.toFixed(0) + ' votos' : scenario.total_votes_equalized == 1 ? scenario.total_votes_equalized + ' voto' : 'Nenhum voto ainda'}}</h5>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12 d-none d-sm-block fade-right">
                                <img class="img-fluid" src="{{asset('/img/mapa_brasil.png')}}" alt="">
                            </div>
                            <div class="col-sm-12 fade-left">
                                <div class="result-list">
                                    <div v-for="candidate in _.reverse(_.sortBy(scenario.candidates,['votes_equalized','name']))" class="result-item"
                                        :title="candidate.votes_equalized > 1 ? candidate.votes_equalized + ' votos' : candidate.votes_equalized == 1 ? candidate.votes_equalized + ' voto' : 'Nenhum voto ainda'">
                                        <div class="result-item-image">
                                            <img class="rounded-circle img-fluid" :src="'/img/candidates/' +  candidate.image" alt="">
                                        </div>
                                        
                                        <div class="result-item-right">
                                            <div class="result-item-name">
                                                @{{candidate.name}}
                                            </div>
                                            <div class="result-item-bar">
                                                <div class="result-item-bar-votes" :style="{width: (scenario.total_votes_equalized > 0 ? candidate.votes_equalized * 100 / scenario.total_votes_equalized : 0)+'%'}"></div>
                                            </div>
                                            <div class="result-item-number">@{{(scenario.total_votes_equalized > 0 ? candidate.votes_equalized * 100 / scenario.total_votes_equalized : 0).toFixed(1)+'%'}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <template v-for="(region, r) in scenario.regions">
                    <div class="py-8 pt-5 " :class="{'bg-grey': r%2!=0}">
                        <div class="container">
                            <div class="row justify-content-center text-center">
                                <div class="col title">
                                    <h2 class="mb-5"><b>@{{region.name}}</b></h2>
                                    <h5>@{{region.total_votes_equalized > 1 ? region.total_votes_equalized.toFixed(0) + ' votos' : region.total_votes_equalized == 1 ? region.total_votes_equalized + ' voto' : 'Nenhum voto ainda'}}</h5>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-12 d-none d-sm-block fade-right">
                                    <img class="img-fluid" :src="'/img/mapa_' + region.slug + '.png'" alt="">
                                </div>
                                <div class="col-sm-12 fade-left">
                                    <div class="result-list">
                                        <div v-for="candidate in _.reverse(_.sortBy(region.candidates,['votes_equalized','name']))" class="result-item"
                                            :title="candidate.votes_equalized > 1 ? candidate.votes_equalized.toFixed(0) + ' votos' : candidate.votes_equalized == 1 ? candidate.votes_equalized + ' voto' : 'Nenhum voto ainda'">
                                            <div class="result-item-image">
                                                <img class="rounded-circle img-fluid" :src="'/img/candidates/' +  candidate.image" alt="">
                                            </div>
                                            
                                            <div class="result-item-right">
                                                <div class="result-item-name">
                                                    @{{candidate.name}}
                                                </div>
                                                <div class="result-item-bar">
                                                    <div class="result-item-bar-votes" :style="{width: (region.total_votes_equalized > 0 ? (candidate.votes_equalized * 100 / region.total_votes_equalized).toFixed(1) : 0)+'%'}"></div>
                                                </div>
                                                <div class="result-item-number">@{{(region.total_votes_equalized ? (candidate.votes_equalized * 100 / region.total_votes_equalized).toFixed(1) : 0)+'%'}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </template>
                <template>
                    <div class="py-8 pt-5 bg-grey">
                        <div class="container">
                            <div class="row justify-content-center text-center">
                                <div class="col title">
                                    <h2 class="mb-5"><b>POR GÊNERO</b></h2>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-12 fade-right">
                                    <h3 class="text-center"><b>ELAS</b></h3>
                                    <h5 class="text-center">@{{scenario.total_women_votes > 1 ? scenario.total_women_votes + ' votos' : scenario.total_women_votes == 1 ? scenario.total_women_votes + ' voto' : 'Nenhum voto ainda'}}</h5>
                                    <hr>
                                    <div class="result-list">
                                        <div v-for="candidate in _.reverse(_.sortBy(scenario.candidates,['women_votes','name']))" class="result-item"
                                        :title="candidate.women_votes > 1 ? candidate.women_votes + ' votos' : candidate.women_votes == 1 ? candidate.women_votes + ' voto' : 'Nenhum voto ainda'">
                                            
                                            <div class="result-item-right">
                                                <div class="result-item-name">
                                                    @{{(scenario.total_women_votes > 0 ? candidate.women_votes * 100 / scenario.total_women_votes : 0).toFixed(1)+'%'}}
                                                </div>
                                                <div class="result-item-bar" >
                                                    <div class="result-item-bar-votes" :style="{width: (scenario.total_women_votes > 0 ? candidate.women_votes * 100 / scenario.total_women_votes : 0)+'%'}" style="right:0; left: auto; background:#e23ebf; "></div>
                                                </div>
                                                <div class="result-item-number">
                                                    @{{candidate.name}}
                                                </div>
                                            </div>
                                            <div class="result-item-image text-right">
                                                <img class="rounded-circle img-fluid" :src="'/img/candidates/' +  candidate.image" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 fade-left">
                                    <h3 class="text-center"><b>ELES</b></h3>
                                    <h5 class="text-center">@{{scenario.total_men_votes > 1 ? scenario.total_men_votes + ' votos' : scenario.total_men_votes == 1 ? scenario.total_men_votes + ' voto' : 'Nenhum voto ainda'}}</h5>
                                    <hr>
                                    <div class="result-list">
                                        <div v-for="candidate in _.reverse(_.sortBy(scenario.candidates,['men_votes','name']))" class="result-item"
                                        :title="candidate.men_votes > 1 ? candidate.men_votes + ' votos' : candidate.men_votes == 1 ? candidate.men_votes + ' voto' : 'Nenhum voto ainda'">
                                            <div class="result-item-image">
                                                <img class="rounded-circle img-fluid" :src="'/img/candidates/' +  candidate.image" alt="">
                                            </div>
                                            
                                            <div class="result-item-right">
                                                <div class="result-item-name">
                                                    @{{candidate.name}}
                                                </div>
                                                <div class="result-item-bar">
                                                    <div class="result-item-bar-votes" :style="{width: (scenario.total_men_votes > 0 ? candidate.men_votes * 100 / scenario.total_men_votes : 0)+'%'}" style="background:#2a8ee4;"></div>
                                                </div>
                                                <div class="result-item-number">@{{(scenario.total_men_votes > 0 ? candidate.men_votes * 100 / scenario.total_men_votes : 0 ).toFixed(1)+'%'}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
        </template>
    </div>
</section>
@endsection