@extends('layouts.app', ['component'=>'home'])

@section('content')
<section class="container">
    <div class="row">
        <div class="col">
            <br>
            <h2 class="mt-4">Política de Privacidade</h2>
            <div class="mt-4 card card-primary">
                <div class="card-body">
                    <h5 class="card-title">
                        <h4>Informações que coletamos e armazenamos</h4>
                    </h5>
                    <p>Nós coletamos e armazenamos somente as informações fornecidas pelos usuários. <br>São elas:</p>
                    <ul>
                        <li>Nome;</li>
                        <li>Email;</li>
                        <li>Senha (criptografada);</li>
                        <li>Estado;</li>
                        <li>Cidade;</li>
                        <li>Gênero;</li>
                        <li>Data de nascimento;</li>
                    </ul>
                    <p>Os votos individuais fornecidos pelos usuários são usados para a geração de estatísticas e JAMAIS serão fornecidos a terceiros estando vinculados às informações pessoais que possam indentificar a identidade do votante.</p>
                    <p><b>Nenhuma das informações pessoais dos usuários será compartilhada, vendida ou fornecida a terceiros.</b></p>
                </div>
            </div>
            <br>
            <h2 class="mt-4">Termos de uso</h2>
            <div class="mt-4 card card-primary">
                <div class="card-body">
                    <h5 class="card-title">
                        <h4>Ao criar a sua conta você concorda que:</h4>
                    </h5>
                    <ul>
                        <li>Não há outra conta criada por você para somar votos para o candidato de sua preferência;</li>
                        <li>Não tentará, de nenhuma maneira, fraudar os votos ou corromper o sistema de votação;</li>
                        <li>Suas informações são verdadeiras e pertencem a você;</li>
                        <li>Seu email é válido e, se necessário, poderemos entrar em contato através dele;</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
