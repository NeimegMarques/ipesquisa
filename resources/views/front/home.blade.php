@extends('layouts.app', ['component'=>'home'])

@section('content')
<section class="container surveys-index">
    <div class="row">
        <div class="col">
            <br>
            <br class="d-none d-sm-block">
            <h2 class="mb-3 text-xs-center">PESQUISAS</h2>
            <br class="d-none d-sm-block">
            @foreach($surveys as $i => $survey)
                <div class="survey">
                    <a href="{{url('/pesquisa/' . $survey->uuid)}}">
                        <h3 class="survey-question">{{$survey->question}}</h3>
                        <ul class="survey-candidates-list">
                            @foreach($survey->candidates->shuffle() as $candidate)
                            <li>
                                <img class="img-fluid rounded-circle" src="{{asset('/img/candidates/' .  $candidate->image)}}" alt="">
                            </li>
                            @endforeach
                        </ul>
                    </a>
                </div>
                @if($i%5 == 0)
                    <div class="mb-5">
                        <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1955889356284494"
                         data-ad-slot="4179029457"
                         data-ad-format="auto"></ins>
                    </div>
                @endif
            @endforeach()
        </div>
    </div>
</section>
@endsection
