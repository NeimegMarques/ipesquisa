@extends('layouts.app', ['component'=>'home'])

@section('content')
<section class="">
    <div class="row">
        <div class="col">
            <ul class="list-group">
                @foreach($surveys as $survey)
                <li class="list-group-item">
                    <a href="{{url('/pesquisa/' . $survey->uuid)}}">
                        {{$survey->question}}
                    </a>
                </li>
                @endforeach()
            </ul>
        </div>
    </div>
</section>
@endsection
