@extends('layouts.auth', ['component'=>'register-page'])

@section('content')
<div class="container">
    <div class="card card-container">
        <h4 class="text-center mb-0"><b>{{ __('Cadastrar') }}</b></h4>
        <template v-if="errors.length > 0">
            <div class="errors mt-3">
                <b>Por favor, corrija os seguintes erros:</b>
                <ul>
                    <li v-for="error in errors">@{{ error }}</li>
                </ul>
            </div>
        </template>
        @if(count($errors->all()))
            <div class="errors mt-3">
                <b>Os erros abaixo nos impedem de prosseguir :(</b>
                @foreach ($errors->all() as $error)
                    <ul>
                        <li>{{ $error }}</li>
                    </ul>
                @endforeach
            </div>
        @endif

        
        <a style="background: #5547B4;" href="{{ url('auth/facebook') }}" class="mt-5 btn btn-lg btn-primary btn-block mb-1">
            <i style="font-size: 18px;" class="fa fa-facebook-square"></i> 
            <b class="ml-2">Cadastrar usando o Facebook</b>
        </a>


        <form method="POST" action="{{ route('register') }}" class="form-signin" @submit="checkForm" novalidate="true">
            @csrf
            <p class="mt-4 mb-2"><b>Informações básicas</b></p>
            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Nome" v-model="register.name" type="text" class="form-control form-control-lg{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Email" v-model="register.email" type="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Senha" v-model="register.password" type="password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Confirme a senha" v-model="register.password_confirmation" type="password" class="form-control form-control-lg" name="password_confirmation" required>
                </div>
            </div>

            <p class="mt-4 mb-2"><b>Localização</b></p>
            <div class="form-group row">
                <div class="col-md-24">
                    <select class="form-control form-control-lg{{ $errors->has('state') ? ' is-invalid' : '' }}" v-model="register.state" name="state" value="{{ old('state') }}" required autofocus>
                        <option value="" disabled>Estado</option>
                        @foreach(\App\Models\State::all() as $state)
                        <option value="{{$state->id}}">{{$state->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-24">
                    <select v-model="register.city" class="form-control form-control-lg{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required autofocus>
                        <option value="" disabled selected>Cidade</option>
                        <template v-for="city in cities.data">
                            <option v-if="register.state != ''" :value="city.id">@{{city.name}}</option>
                        </template>
                    </select>
                </div>
            </div>

            <p class="mt-4 mb-2"><b>Gênero</b></p>
            <div class="form-group row">
                <div class="col-md-24">
                    <select v-model="register.gender" class="form-control form-control-lg{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" value="{{ old('gender') }}" required autofocus>
                        <option value="" disabled selected>Gênero</option>
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                        <option value="O">Outro</option>
                    </select>
                </div>
            </div>

            <p class="mt-4 mb-2"><b>Nascimento</b></p>
            <div class="form-group row">
                <div class="col-7">
                    <select v-model="register.birth_day" class="form-control form-control-lg{{ $errors->has('birth_day') ? ' is-invalid' : '' }}" name="birth_day" value="{{ old('birth_day') }}" required autofocus>
                        <option value="" disabled selected>Dia</option>
                        @for($i = 1; $i<=31; $i++)
                            <option value="{{($i<=9 ? '0':'').$i}}">{{($i<=9 ? '0':'').$i}}</option>
                        @endfor
                    </select>
                </div>
                <div class="col-7">
                    <select v-model="register.birth_month" class="form-control form-control-lg{{ $errors->has('birth_month') ? ' is-invalid' : '' }}" name="birth_month" value="{{ old('birth_day') }}" required autofocus>
                        <option value="" disabled selected>Mês</option>
                        @for($i = 1; $i<=12; $i++)
                            <option value="{{($i<=9 ? '0':'').$i}}">{{($i<=9 ? '0':'').$i}}</option>
                        @endfor
                    </select>
                </div>
                <div class="col-10">
                    <select v-model="register.birth_year" class="form-control form-control-lg{{ $errors->has('birth_year') ? ' is-invalid' : '' }}" name="birth_year" value="{{ old('birth_day') }}" required autofocus>
                        <option value="" disabled selected>Ano</option>
                        @for($i = date('Y') - 14; $i>=date('Y') - 114; $i--)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <br>
            <br>
            <div class="form-group row mb-0">
                <div class="col-md-24">
                    <span>
                        <a href="{{url('/login')}}" class="forgot-password mt-3 d-inline-block">
                            Já tem uma conta?
                        </a>
                    </span>
                    <button :disabled="saving" @click="saving=true" type="submit" class="btn btn-primary btn-lg float-right">
                        <span v-if="saving">Cadastrando...</span>
                        <span v-else>{{ __('Cadastrar') }}</span>
                    </button>
                </div>
            </div>
            <hr>
            <div class="form-group row mb-0">
                <div class="col-md-24 text-center">
                    <span>
                        Ao cadastrar-se você concorda com nossos 
                        <a target="_blank" href="{{url('/privacidade_e_termos')}}" class="d-inline-block">
                            termos de uso e privacidade
                        </a>
                    </span>
                </div>
            </div>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->
@endsection
