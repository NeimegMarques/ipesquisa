@extends('layouts.auth', ['component'=>'login-page'])

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-8 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-8 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 offset-md-8">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-16 offset-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 -->

<div class="container">
    <div class="card card-container">
        <h4 class="text-center mb-5"><b>{{ __('Entrar') }}</b></h4>
        <template v-if="errors.length > 0">
            <div class="errors mt-3">
                <ul>
                    <li v-for="error in errors">@{{ error }}</li>
                </ul>
            </div>
        </template>
        @if(count($errors->all()))
            <div class="errors mt-3">
                @foreach ($errors->all() as $error)
                    <ul>
                        <li>{{ $error }}</li>
                    </ul>
                @endforeach
            </div>
        @endif
        <form method="POST" action="{{ route('login') }}" class="form-signin" @submit="checkForm" novalidate="true">
            @csrf

            <a style="background: #5547B4;" href="{{ url('auth/facebook') }}" class="btn btn-lg btn-primary btn-block mb-5">
                <i style="font-size: 18px;" class="fa fa-facebook-square"></i> 
                <b class="ml-2">Entrar usando o Facebook</b>
            </a>
            
            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Email" v-model="login.email" type="email" class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-24">
                    <input placeholder="Senha" v-model="login.password" type="password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                </div>
            </div>
            <br>
            <br>
            <div class="form-group row mb-0">
                <div class="col-md-24">
                    <span>
                        <a href="{{url('/register')}}" class="mt-2 d-inline-block">
                            Não tem uma conta?
                        </a>
                    </span>
                    <button type="submit" class="btn btn-primary btn-lg float-right">
                        {{ __('Entrar') }}
                    </button>
                </div>
                <div class="col-md-24">
                    <a href="{{url('/password/reset')}}" class="float-right mt-3 d-inline-block">
                        Esqueceu a senha?
                    </a>
                </div>
            </div>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->
@endsection
