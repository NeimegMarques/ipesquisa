<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth" content="{{@Auth::check()}}">
    <meta name="verified" content="{{@Auth::user()->verified}}">
    <meta name="user" content="{{@Auth::user()}}">
    <meta name="states" content="{{@\App\Models\State::all()}}">

    <title>iPesquisa - {{@$meta['title'] ?:'Se as eleições fossem hoje, para qual candidato você daria o seu voto?'}}</title>

    <meta name="description" content="{{@$meta['description'] ?:'Ajude-nos a descobrir qual candidato tem mais chances de vencer nessas eleições. Vote!'}}">
    <meta name="keywords" content="eleições, pesquisa, opinião, voto, candidatos">
    <link rel="author" href="iPesquisa.org" />
    <link rel="canonical" href="{{@$meta['url'] ?:'http://ipesquisa.org'}}" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">

    <!-- FACEBOOK -->
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="627">
    <meta property="og:image" content="{{asset('/candidatos.jpg')}}">
    <meta property="og:image:type" content="image/jpeg">


    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="379241195812736">
    
    <meta property="og:description" content="{{@$meta['description'] ?:'Ajude-nos a descobrir qual candidato tem mais chances de vencer nessas eleições. Vote!'}}">
    <meta property="og:url" content="{{@$meta['url'] ?:'http://ipesquisa.org'}}">
    <meta property="og:title" content="{{@$meta['title'] ?:'Se as eleições fossem hoje, para qual candidato você daria o seu voto?'}}">
    <meta property="og:site_name" content="iPesquisa">

    <!-- GOOGLE -->
    <meta itemprop="name" content="iPesquisa - {{@$meta['title'] ?:'Se as eleições fossem hoje, para qual candidato você daria o seu voto?'}}">
    <meta itemprop="description" content="{{@$meta['description'] ?:'Ajude-nos a descobrir qual candidato tem mais chances de vencer nessas eleições. Vote!'}}">
    <meta itemprop="image" content="{{asset('/candidatos.jpg')}}">

    <!-- TWITTER -->
    <meta name="twitter:card" content="iPesquisa - {{@$meta['title'] ?:'Se as eleições fossem hoje, para qual candidato você daria o seu voto?'}}">
    <meta name="twitter:url" content="{{@$meta['url'] ?:'http://ipesquisa.org'}}">
    <meta name="twitter:title" content="iPesquisa - {{@$meta['title'] ?:'Se as eleições fossem hoje, para qual candidato você daria o seu voto?'}}">
    <meta name="twitter:description" content="{{@$meta['description'] ?:'Ajude-nos a descobrir qual candidato tem mais chances de vencer nessas eleições. Vote!'}}">
    <meta name="twitter:image" content="{{asset('/candidatos.jpg')}}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    @if(strpos(Request::url(), 'eleicoes.site'))
        
    @else
        <script type="text/javascript">
          window._mfq = window._mfq || [];
          (function() {
            var mf = document.createElement("script");
            mf.type = "text/javascript"; mf.async = true;
            mf.src = "//cdn.mouseflow.com/projects/ae70548d-2e5a-46ef-b48d-627b818cce02.js";
            document.getElementsByTagName("head")[0].appendChild(mf);
          })();
        </script>


        <!-- FACEBOOK -->
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '379241195812736',
              xfbml      : true,
              version    : 'v2.12'
            });
          
            FB.AppEvents.logPageView();
          
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>


        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2082466392022592');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=2082466392022592&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->


        <!-- END FACEBOOK -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117403270-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-117403270-1');
        </script>

        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        
    @endif


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="{{Auth::check() && !Auth::user()->verified ? 'not-verified':''}}">
    <div id="app">
        @if(Auth::check() && !Auth::user()->verified)
        <div class="not-verified-bar">
            <div class="container">
                <div class="text-center"><span class="d-none d-sm-inline">Sua conta ainda não foi verificada. </span><a @click="sendVerifEmailClicked($event)" href="{{url('/send-verification-email')}}">Re-enviar email de verificação para {{Auth::user()->email}}</a></div>
            </div>
        </div>
        @endif
        <topbar inline-template>
            @include('front.partials.topbar')
        </topbar>
        <!-- <div class="container">
            @if(session()->has('message'))
                <div class="alert alert-info mt-3 mb-3">
                    {{session('message')}}
                </div>
            @endif
        </div> -->
        <main class="">
            <component is="{{$component}}" flash_message="{{session('message')}}" inline-template
            @if(count(@$props?:[]))
                @foreach($props as $prop => $value)
                {{$prop}} = {{$value}}
                @endforeach
            @endif
            >
                @yield('content')
            </component>
        </main>
        <component is="missing-user-info">
        </component>
    </div>

    @if ( Config::get('app.debug') )
      <script type="text/javascript">
        document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
      </script>
    @endif
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</body>
</html>
