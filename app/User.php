<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    public static function boot()
    {
        parent::boot();

        static::creating( function ($user) {
            $user->token = str_random(30);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'state_id',
        'city_id',
        'gender',
        'verified',
        'birth',
        'provider',
        'provider_id',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function confirmEmail()
    {
        $this->verified = 1;
        $this->token = null;
        $this->save();
    }

    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }

        return $check;
    }

    public function getAvatarImageAttribute(){
        return $this->avatar?:asset('/img/users/user.jpg');
    }

}
