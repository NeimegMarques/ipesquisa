<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index(Request $request){
    	$state_id = $request->state_id;
    	if($state_id){
    		return \App\Models\City::where('state_id', $state_id)->get();
    	} else {
    		return \App\Models\City::paginate(40);
    	}
    }
}
