<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function index()
    {
        $surveys = \App\Models\Survey::all();
        return view('front.home')->withSurveys($surveys);
    }

    public function sendVerificationEmail(){
        $user = \Auth::user();
        \Illuminate\Support\Facades\Mail::to($user->email)->send(new \App\Mail\VerificationEmail($user));
        session()->flash('message','Enviamos um email de verificação de conta para você.');
        return redirect()->back();
    }

    public function verifyEmail($token){
        $user = \App\User::whereToken($token)->firstOrFail()->confirmEmail();
        session()->flash('message','Sua conta foi verificada com sucesso.');
        return redirect('/');
    }

    public function privacy(){
        return view('front.privacy');
    }
}
