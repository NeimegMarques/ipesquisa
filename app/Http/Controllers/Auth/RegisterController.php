<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'bail|required|string|email|max:255|unique:users|not_throw_away',
            'password' => 'required|string|min:6|confirmed',
            'state' => 'required|int',
            'city' => 'required|int',
            'gender' => 'required|string|max:1',
            'birth_day' => 'required|string|max:2',
            'birth_month' => 'required|string|max:2',
            'birth_year' => 'required|string|max:4'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'state_id' => $data['state'],
            'city_id' => $data['city'],
            'gender' => $data['gender'],
            'birth' => $data['birth_year'].'-'.$data['birth_month'].'-'.$data['birth_day']
        ]);
        session()->flash('message','Estamos quase lá. Agora você só precisa validar sua conta através do email que te enviamos.');
        Mail::to($user->email)->send(new UserRegistered($user));

        return $user;
    }


    public function showRegistrationForm()
    {
        if(request()->intended != ''){
            session()->put('intended', url(request()->intended));
            return redirect('/register');
        } else {
            return view('auth.register');
        }

    }

    protected function registered(Request $request, $user)
    {
        return redirect(session()->pull('intended',$this->redirectTo));
    }
}
