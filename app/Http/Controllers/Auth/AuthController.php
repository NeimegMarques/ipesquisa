<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;

class AuthController extends Controller
{

	public $redirectTo = "/";

	public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user,$provider);

        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
        	if(!$authUser->avatar){
        		$authUser->avatar = $user->avatar;
        		$authUser->save();
        	}
        	
        	Auth::login($authUser, true);
            return $authUser;
        }
        $user = User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'password' => \Hash::make(str_random(10)),
            'verified' => true,
            'gender' => strtoupper(substr($user->user['gender'],0,1)),
            'avatar'    => $user->avatar,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);

        Auth::login($user, true);

        return $user;
    }
}
