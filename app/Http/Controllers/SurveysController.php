<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Survey;

class SurveysController extends Controller
{
	private $city_id = null;
	private $state_id = null;
	private $peso_homens = 1;
	private $peso_mulheres = 1;

    public function show($uuid){

    	$survey = Survey::where('uuid', $uuid)
    		->with('campaign')
    		->with('scenarios')
    		->first();

    	$voted = \App\Models\Vote
    		::where('campaign_id',$survey->campaign->id)
    		->where('user_id',@\Auth::user()->id)
    		->first();
    	
    	return view('front.survey')->withSurvey($survey)->withVoted($voted?true:false);
    }

    public function APIshow($uuid){
    	if(\Request::has('state_id')) $this->state_id = \Request::get('state_id');
    	if(\Request::has('city_id')) $this->city_id = \Request::get('city_id');
    	
    	$survey = Survey::where('uuid', $uuid)
    		->with('campaign')
    		->with('campaign.votes')
    		->with('scenarios')
    		->first();

    	if($this->state_id){
    		$votes = $survey->campaign->votes->where('user.state_id',$this->state_id)->load('user');
    	} elseif($this->city_id){
    		$votes = $survey->campaign->votes->where('user.city_id',$this->city_id)->load('user');
    	} else{
    		$votes = $survey->campaign->votes->load('user');
    	}

    	$candidates = $survey->candidates;
    	$states = \App\Models\State::all();

    	$survey->scenarios->reverse()->map( function($scenario) use ($votes, $candidates, $states){
    		$this->peso_homens = 1;
    		$this->peso_mulheres = 1;

    		$excluded_ids = json_decode($scenario->exclude);
			
    		$cands = collect();
    		foreach ($candidates as $key => $cand) {
    			if( !in_array($cand->id, $excluded_ids)){
    				$cands->push(clone $cand);
    			}
    		}
    		$scenario->candidates = $cands;

			$scenario_votes = $votes->filter(function ($vote, $key) use($scenario) {
			    return $vote->scenario_id  == $scenario->id;
			});


			$scenario_men_votes = $scenario_votes->filter(function ($vote, $key) {
			    return $vote->user->gender == 'M';
			});

			$scenario_women_votes = $scenario_votes->filter(function ($vote, $key) {
			    return $vote->user->gender == 'F';
			});

			$scenario->total_men_votes = $scenario_men_votes->count();
			$scenario->total_women_votes = $scenario_women_votes->count();
			$scenario->total_votes = $scenario_votes->count();

			

			//NÃO É O TOTAL DE VOTOS REAL, É O TOTAL * PESO
			if($scenario->total_men_votes >= $scenario->total_women_votes){
				$this->peso_mulheres = $scenario->total_women_votes > 0 ? $scenario->total_men_votes / $scenario->total_women_votes : $scenario->total_men_votes;
			} else {
				$this->peso_homens = $scenario->total_men_votes > 0 ? $scenario->total_women_votes / $scenario->total_men_votes : $scenario->total_women_votes;
			}
			
			$scenario->total_votes_equalized = $scenario->total_women_votes * $this->peso_mulheres + $scenario->total_men_votes * $this->peso_homens;

			$scenario->candidates->map( function($candidate) use ($scenario, $scenario_votes){
				$candidate->men_votes = 0;
				$candidate->men_votes_equalized = 0;

				$candidate->women_votes = 0;
				$candidate->women_votes_equalized = 0;
				
				$votos = $scenario_votes->filter(function ($vote, $key) use($scenario, $candidate) {
				    return $vote->candidate_id == $candidate->id;
				});

				$votos->each(function($vote) use($candidate){
					if($vote->user->gender == 'M'){
						$candidate->men_votes++;
					} elseif($vote->user->gender == 'F'){
						$candidate->women_votes++;
					}
				});

				$candidate->men_votes_equalized = $candidate->men_votes * $this->peso_homens;
				$candidate->women_votes_equalized = $candidate->women_votes * $this->peso_mulheres;

				$candidate->votes = $candidate->women_votes + $candidate->men_votes;
				$candidate->votes_equalized = $candidate->women_votes_equalized + $candidate->men_votes_equalized;

				return $candidate;
			});

			if(!$this->city_id && !$this->state_id){
				$regions = collect();
			
				$sudeste = new \stdClass(); 	
			 	$sudeste->slug = 'sudeste';
				$sudeste->total_votes = 0;
				$sudeste->total_votes_equalized = 0;
				$sudeste->name = 'Sudeste';
				$sudeste->candidates =  collect();
				$sudeste->states  =  [8,19,26,11];
					
				$nordeste = new \stdClass();
				$nordeste->slug =  'nordeste';
				$nordeste->total_votes = 0;
				$nordeste->total_votes_equalized = 0;
				$nordeste->name = 'Nordeste';
				$nordeste->candidates =  collect();
				$nordeste->states  =  [2,5,6,10,15,16,17,20,25];

				$norte = new \stdClass();
				$norte->slug = 'norte';
				$norte->total_votes = 0;
				$norte->total_votes_equalized = 0;
				$norte->name = 'Norte';
				$norte->candidates = collect();
				$norte->states = [1,3,4,14,21,22,27];

				$centroeste = new \stdClass();
				$centroeste->slug  =  'centroeste';
				$centroeste->total_votes = 0;
				$centroeste->total_votes_equalized = 0;
				$centroeste->name = 'Centro-oeste';
				$centroeste->candidates =  collect();
				$centroeste->states  =  [7,9,12,13];

				$sul = new \stdClass();
				$sul->slug  =  'sul';
				$sul->total_votes = 0;
				$sul->total_votes_equalized = 0;
				$sul->name = 'Sul';
				$sul->candidates =  collect();
				$sul->states  =  [18,23,24];


				$regions->push($norte);
				$regions->push($nordeste);
				$regions->push($centroeste);
				$regions->push($sudeste);
				$regions->push($sul);
				

				$scenario->regions = $regions;


				foreach($scenario->regions as $r => $region){


					$region_votes = $scenario_votes->filter(function ($vote, $key) use($region) {
					    return in_array($vote->user->state_id, $region->states);
					});

	    			$region->total_votes = $region_votes->count();

					foreach($cands as $c => $cand){
	    				$candidate = clone($cand);
	    				$candidate->men_votes = 0;
	    				$candidate->women_votes = 0;
	    				$candidate->votes = 0;
	    				$candidate->votes_equalized = 0;

						$votos = $region_votes->filter(function ($vote, $key) use($cand) {
						    return $vote->candidate_id == $cand->id;
						});

						$votos->each(function($vote) use($candidate){
							if($vote->user->gender == 'M'){
								$candidate->men_votes++;
							} elseif($vote->user->gender == 'F'){
								$candidate->women_votes++;
							}
						});

						$candidate->votes = $candidate->men_votes + $candidate->women_votes;
						$candidate->votes_equalized = $candidate->men_votes * $this->peso_homens + $candidate->women_votes * $this->peso_mulheres;
	    				
	    				$region->total_votes_equalized += $candidate->votes_equalized;
						

						$region->candidates->push($candidate);
					};

				};
			}

			return $scenario;
    	});

    	return $survey;
    }

    public function APIshowByState($uuid,$user_state_id){
    	$survey = Survey::where('uuid', $uuid)
    		->with('campaign')
    		->with('campaign.votes')
    		->with('scenarios')
    		->first();

    	$state_id = $user_state_id >= 1 ? $user_state_id : \Auth::user()->state_id;

    	$votes = $survey->campaign->votes->where('user.state_id',$state_id)->load('user');

    	$candidates = $survey->candidates;
    	$states = \App\Models\State::all();

    	$survey->scenarios->reverse()->map( function($scenario) use ($votes, $candidates, $states){

    		$excluded_ids = json_decode($scenario->exclude);
			
    		$cands = collect();
    		foreach ($candidates as $key => $cand) {
    			if( !in_array($cand->id, $excluded_ids)){
    				$cands->push(clone $cand);
    			}
    		}
    		$scenario->candidates = $cands;

			$scenario_votes = $votes->filter(function ($vote, $key) use($scenario) {
			    return $vote->scenario_id  == $scenario->id;
			});


			$scenario_men_votes = $scenario_votes->filter(function ($vote, $key) {
			    return $vote->user->gender == 'M';
			});

			$scenario_women_votes = $scenario_votes->filter(function ($vote, $key) {
			    return $vote->user->gender == 'F';
			});

			$scenario->total_votes = $scenario_votes->count();
			$scenario->total_men_votes = $scenario_men_votes->count();
			$scenario->total_women_votes = $scenario_women_votes->count();


			$scenario->candidates->map( function($candidate) use ($scenario, $scenario_votes){
				$candidate->men_votes = 0;
				$candidate->women_votes = 0;
				$votos = $scenario_votes->filter(function ($vote, $key) use($scenario, $candidate) {
				    return $vote->candidate_id == $candidate->id;
				});
				$candidate->votes = $votos->count();

				$votos->each(function($vote) use($candidate){
					if($vote->user->gender == 'M'){
						$candidate->men_votes++;
					} elseif($vote->user->gender == 'F'){
						$candidate->women_votes++;
					}
				});

				return $candidate;
			});

			return $scenario;
    	});
    }
}
