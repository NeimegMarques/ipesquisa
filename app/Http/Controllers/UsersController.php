<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function fillMissingInfo(Request $request){
    	$user = $request->user();
    	
        $user->state_id = $request->state;
        $user->city_id = $request->city;
        $user->gender = $request->gender;
        $user->birth = $request->birth_year.'-'.$request->birth_month.'-'.$request->birth_day;


        $user->save();

        session()->flash('message','Bom trabalho! Agora você pode navegar e votar em nosso site.');

        return $user;
    }
}
