<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VotesController extends Controller
{
    public function vote(Request $request){

    	$user = $request->user();
    	$campaign_id = $request->campaign;
    	$selected_options = $request->scenarios;

    	$voted = \App\Models\Vote
    		::where('campaign_id',$campaign_id)
    		->where('user_id',$user->id)
    		->first();

    	if(!$voted){
    		foreach($selected_options as $i => $candidate_id){
	    		$campaign = \App\Models\Campaign::find($campaign_id);

	    		if(!$campaign || $campaign->survey_id != $request->survey){
	    			return response()->json(['error' => 'As informações são inconsistentes'],500);
	    		}

	    		$scenario_name_arr = explode('_',$i);
	    		$scenario_id = $scenario_name_arr[ count($scenario_name_arr) - 1 ];

	    		\App\Models\Vote::create([
	    			'campaign_id' => $campaign->id,
	    			'scenario_id' => $scenario_id,
	    			'candidate_id' => $candidate_id,
	    			'user_id' => $user->id
	    		]);

	    	}

    		return response()->json('Ok',200);
    	} else {
    		
    		return response()->json('Você já votou',200);

    	}

    }
}
