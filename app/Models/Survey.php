<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{

	protected $with = ['candidates'];

    public function campaign(){
    	return $this->hasOne(\App\Models\Campaign::class)->whereNull('ended_at')->latest();
    }

    public function campaigns(){
    	return $this->hasMany(\App\Models\Campaign::class);
    }

    public function scenarios(){
    	return $this->hasMany(\App\Models\Scenario::class);
    }

    public function candidates(){
    	return $this->belongsToMany(\App\Models\Candidate::class)->where('candidate_survey.active',1);
    }
}
