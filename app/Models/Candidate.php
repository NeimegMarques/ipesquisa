<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    public function party(){
    	return $this->belongsTo(\App\Models\Party::class);
    }
}
