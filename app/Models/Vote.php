<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{

    protected $fillable = [
        'campaign_id', 'scenario_id', 'candidate_id','user_id',
    ];

    public function campaign(){
    	return $this->belongsTo(\App\Models\Campaign::class);
    }

    public function scenario(){
    	return $this->belongsTo(\App\Models\Scenario::class);
    }

    public function candidate(){
    	return $this->belongsTo(\App\Models\Candidate::class);
    }

    public function user(){
    	return $this->belongsTo(\App\User::class);
    }
}
