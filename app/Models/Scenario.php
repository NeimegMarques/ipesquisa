<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scenario extends Model
{

	public $appends = ['excluded_ids'];

	public function getExcludedIdsAttribute(){
		$arr = json_decode($this->exclude);

		return $arr?:[];
	}

    public function survey(){
    	return $this->belongsTo(\App\Models\Survey::class);
    }
}
