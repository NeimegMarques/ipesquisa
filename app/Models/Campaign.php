<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Campaign extends Model
{
    public function survey(){
    	return $this->belongsTo(\App\Models\Survey::class);
    }

    public function votes(){
    	$date = new Carbon();
		//$date->subDays(2);
    	$date->startOfWeek();
    	
    	return $this->hasMany(\App\Models\Vote::class)->whereDate('created_at','>=',$date);
    }
}
